<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Material::class, function(Faker\Generator $faker){
	return [
		'type' => $faker->word,
        'grade' => $faker->word,
        'color' => $faker->colorName,
        'notes' => $faker->sentence,
    ];
});

$factory->define(App\Customer::class, function(Faker\Generator $faker){
	return [
		'Customer' => $faker->company,
    ];
});

$factory->define(App\Manufacturer::class, function(Faker\Generator $faker){
	return [
		'manuname' => $faker->randomLetter . $faker->randomLetter . $faker->randomLetter . "-" . $faker->company,
    ];
});

