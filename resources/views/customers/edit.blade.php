@extends('layouts.app')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Edit this Customer</h3>
		</div>
		<div class="panel-body">
			@if (count($errors) > 0)
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			<form action="/customers/{{$customer->id }}" method="POST" style="display:inline;">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				
					<div class="form-group">
					    <label for="customer">Customer Name:</label>
					    <input autofocus type="text" class="form-control" id="customer" name="customer" value="{{ $customer->customer }}">
					</div>
					<a href="/customers" class="btn btn-warning">< Back</a>
					<button type="Submit" class="btn btn-primary">Update</button>
					
				
			</form>
			<form action="/customers/{{ $customer->id }}" method="POST" style="display:inline;">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								
									<button type="Submit" class="btn btn-danger">Delete</button>
								
									
							</form>
		</div>
	</div>
	
@endsection