@extends('layouts.app')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Create New Customer</h3>
		</div>
		<div class="panel-body">
			@if (count($errors) > 0)
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			<form action="/customers" method="POST">
				{{ csrf_field() }}
				<fieldset>
					<div class="form-group">
					    <label for="customer">Customer Name:</label>
					    <input autofocus type="text" class="form-control" id="customer" name="customer" value="">
					</div>
					
					<button type="Submit" class="btn btn-primary">Create</button>
				</fieldset>
			</form>
		</div>
	</div>

@endsection