@extends('layouts.app')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">All Customers</h3>
		</div>
		<div class="panel-body">
			<a href="/customers/create" class="btn btn-primary">Add New Customer</a>
			<hr>
			
			<ul>
				@foreach($customers as $customer)
					
						<li><a href="/customers/{{ $customer->id }}/edit">{{ $customer->customer }}</a></li>
						
						
					
				@endforeach
				
			</ul>

			{{ $customers->links() }}
		</div>
	</div>
	
@endsection