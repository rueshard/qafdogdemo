@extends('layouts.app')

@section('content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Quality Assurance Forms : Document Object Generator</div>

                <div class="panel-body">
                   
                    <p>
                        Change Log: 
                        <ul>
                            <li>Mar. 8, 2018</li>
                            <ol>
                                <li>User Ability to Create, Update, and Delete:</li>
                                <ul>
                                    <li>Customers</li>
                                    <li>Manufacturers</li>
                                </ul>
                                <li>Preview Document is now generated on a New Tab.</li>
                            </ol>
                            <li>Mar. 11, 2018</li>
                            <ol>
                                <li>User Ability to Create, Update, and Delete:</li>
                                <ul>
                                    

                                    <li>Materials</li>
                                    <ul>
                                        <li>Materials CRUD is Very Basic. Will expand in the near future for Better User Interface.</li>
                                    </ul>
                                </ul>
                            </ol>
                            <li>Mar. 12, 2018</li>
                                <ol>
                                    <li>Materials Interface has been updated</li>
                                </ol>
                            <li>Mar. 13, 2018</li>
                                <ol>
                                    <li>added sanitizer to form inputs</li>
                                    <li>added simple validation to input</li>
                                    <li>Simplified Document Generation options</li>
                                        <ul>
                                            <li>only a print button which opens in a new tab and automaticly calls print dialog.</li>
                                        </ul>
                                    <li>will be working on a direct email functionality.</li>
                                </ol>
                            <li>Mar. 23, 2018</li>
                                <ol>
                                    <li>removed auto print dialog for UL's only</li>
                                        <ul>
                                            <li>observed, issues with UL image not being printed</li>
                                            <li>will try to fix</li>
                                        </ul>
                                </ol>
                            <li>Mar. 25, 2018</li>
                                <ol>
                                    <li>Fixed UL image automatic print</li>
                                </ol>
                        </ul>
                    </p>
                    
                    <a href="{{ url('/home') }}" class="btn btn-primary">Build a Document</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
