@extends('layouts.app')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">All Manufacturers</h3>
		</div>
		<div class="panel-body">
			<a href="/manufacturers/create" class="btn btn-primary">Add New Manufacturer</a>
			<hr>
			<ul>
				@foreach($manufacturers as $manufacturer)
					<li><a href="/manufacturers/{{ $manufacturer->id }}/edit">{{ $manufacturer->manuname }}</a></li>
				@endforeach
			</ul>
			{{ $manufacturers->links() }}
		</div>
	</div>

@endsection