@extends('layouts.app')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Edit this Manufacturer</h3>
		</div>
		<div class="panel-body">
			@if (count($errors) > 0)
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			<form action="/manufacturers/{{ $manufacturer->id }}" method="POST" style="display:inline;">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}

				<div class="form-group">
				    <label for="manucode">Manufacturers Code:</label>
				    <input type="text" class="form-control" id="manucode" name="manucode" value="{{ $manucode }}">
				</div>
				<div class="form-group">
				    <label for="manuname">Manufacturers Name:</label>
				    <input type="text" class="form-control" id="manuname" name="manuname" value="{{ $manufacturersName }}">
				</div>
				<a href="/manufacturers" class="btn btn-warning">< Back</a>
				<button type="Submit" class="btn btn-primary">Update</button>

			</form>
			<form action="/manufacturers/{{ $manufacturer->id }}" method="POST" style="display:inline;">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}
				
					<button type="Submit" class="btn btn-danger">Delete</button>
				
					
			</form>
		</div>
	</div>

@endsection