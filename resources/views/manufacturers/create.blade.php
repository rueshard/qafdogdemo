@extends('layouts.app')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Create New Manufacturer</h3>
		</div>
		<div class="panel-body">
			@if (count($errors) > 0)
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			<form action="/manufacturers" method="POST">
				{{ csrf_field() }}

				<div class="form-group">
				    <label for="manucode">Manufacturers Code:</label>
				    <input type="text" class="form-control" id="manucode" name="manucode" value="">
				</div>
				<div class="form-group">
				    <label for="manuname">Manufacturers Name:</label>
				    <input type="text" class="form-control" id="manuname" name="manuname" value="">
				</div>

				<button type="Submit" class="btn btn-primary">Create</button>
			</form>
		</div>
	</div>
@endsection