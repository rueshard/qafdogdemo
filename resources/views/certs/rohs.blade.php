<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>RoHs Certificate of Compliance</title>
	<script type="text/javascript">
 		window.onload = function() { window.print(); }
	</script>
</head>
<body>
	

<center><h2>{ Company Name }</h2>



<u><strong>RoHS Certificate of Compliance </strong></u><br>
<small>{ Form Number }</small><br><br>

Date: {{ $data['date'] }}<br>
CUSTOMER: {{$data['customer']}}<br>
PO#: {{ $data['PO'] }}<br>
PART NUMBER: {{ $data['PN'] }}<br>
REVISION: {{ $data['revision'] }}<br>
MATERIAL: {{ $data['material'] }}<br>
QUANTITY: {{ $data['quantity'] }}<br>
<h6>
<p>
We Certify that material has been inspected and found to be in compliance with applicable drawings, specifications and purchase order requirements.
</p>
<p>
<!--
Aurora Technologies, Inc. currently has on file, RoHS letters of compliance from its vendors of the raw material used to make the above part number. These letters state that none of the restricted materials listed under European Union Directive 2002/95/EC, EU Directive 2003/11/EC, EU Directive 76/769 EEC and recast Directive 2011/65/EU for RoHS2, which supersedes Directive 2002/95/EC, are used in the manufacturing. This is including Effective July 1, 2008 the RoHS Directive which now includes the flame retardant decabromodiphenyl ether (DecaBDE). This substance was previously exempt from the directive.
-->
{ Company Name } currently has on file, RoHS letters of compliance from its vendors of the raw material used to make the above part number. These letters state that none of the restricted substances listed under European Union Directive 2002/95/EC, EU Directive 2003/11/EC, EU Directive 76/769 EEC, Directive 2011/65/EU and amendment to annex ll by Directive (EU) 2015/863.
</p>
<p>
	<u>{{ $data['name'] }}</u>
</p>
<small>
<p>
{Company Name} - 101 Industrial Dr - Hereville, {State} - {Zip} - Phone 111.555.1234</p>
</small></center></h6>
</body>
</html>