<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>UL</title>
	<link rel="stylesheet" href="/css/print.css" media="print">
	<link rel="stylesheet" href="/css/print.css">
	
</head>
<body>

	<div class="print_header">
			<img class='image_block' src="/images/img_ul.png" width="140px" height="140px"alt="UL Image">
	<div class="header_block">
		<center>
		<h2>{ Company Name }</h2>

		<u><strong>Component - Fabricated Parts (Plastics):<br>
		(Covered under traceability program)<br>
		Assigned Designation: B-1695 - File E201129</strong></u><br>
		<small>{ Form Number }</small></center>
	</div>

		<br><br>
		<div class="body_block">
			<center>
			Date: {{ $data['date'] }}<br>
			PO#: {{ $data['PO'] }}<br>
			PART NUMBER: {{ $data['PN'] }}<br>
			REVISION: {{ $data['revision'] }} <br>
			MATERIAL MANUFACTURER: {{ $data['manufactureedit'] }}<br>
			MATERIAL DESIGNATION: {{ $data['material'] }} <br>
			<h6>

			<p>
			<!--
			Aurora Technologies, Inc. currently has on file, RoHS letters of compliance from its vendors of the raw material used to make the above part number. These letters state that none of the restricted materials listed under European Union Directive 2002/95/EC, EU Directive 2003/11/EC, EU Directive 76/769 EEC and recast Directive 2011/65/EU for RoHS2, which supersedes Directive 2002/95/EC, are used in the manufacturing. This is including Effective July 1, 2008 the RoHS Directive which now includes the flame retardant decabromodiphenyl ether (DecaBDE). This substance was previously exempt from the directive.
			-->
			{ Company Name } currently has on file, RoHS letters of compliance from its vendors of the raw material used to make the above part number. These letters state that none of the restricted substances listed under European Union Directive 2002/95/EC, EU Directive 2003/11/EC, EU Directive 76/769 EEC, Directive 2011/65/EU and amendment to annex ll by Directive (EU) 2015/863.
			</p>
			<small>
			<p>
			{Company Name} - 101 Industrial Dr - Hereville, {State} - {Zip} - Phone 111.555.1234</p>
			</small></p></center></h6>
		</div>

		<script type="text/javascript">
 			window.onload = function() { window.print(); }
		</script>
	</body>
</html>

