<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Certificate of Compliance</title>
	<script type="text/javascript">
 		window.onload = function() { window.print(); }
	</script>
</head>
<body>
	



<center><h2>{ Company Name }</h2>



<u><strong>Certificate of Compliance </strong></u><br>
<small>{Form Number}</small><br><br>

Date: {{ $data['date'] }}<br>
CUSTOMER: {{$data['customer']}}<br>
PO#: {{ $data['PO'] }}<br>
PART NUMBER: {{ $data['PN'] }}<br>
REVISION: {{ $data['revision'] }}<br>
MATERIAL: {{ $data['material'] }}<br>
@if($data['linfield'])
LIN: {{ $data['linfield'] }}<br>
@endif
@if($data['shelflife'])
SHELF LIFE: {{ $data['shelflife'] }}<br>
@endif
QUANTITY: {{ $data['quantity'] }}<br>
<h6>

<p>
We Certify that material has been inspected and found to be in compliance with applicable drawings, specifications and purchase order requirements.
</p>
@if($data['fandf'])

<p>
	NOTE: The recording of false, fictitious or fraudulent statements or entries on this document may be punishable as a felony under Federal Statutes including Federal law, Title 18, Chapter 47.
</p>
@endif
<br>
<br>
<p>
	<u>{{ $data['name'] }}</u>
</p>
<small>
	<p>password protected electronic signature</p>
<p>
{Company Name} - 101 Industrial Dr - Hereville, {State} - {Zip} - Phone 111.555.1234</p>
</small></center></h6>

</body>
</html>