@extends('layouts.app')

@section('content')
	<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Sending Email</div>

                <div class="panel-body">
                	<pre>
                    	{{ var_dump($data) }}
                	</pre>
                	From: {{ Auth::user()->email }}</br>
                	Subject: {{ $data['subject'] }}
                </div>
            </div>
        </div>
    </div>
</div>

@stop