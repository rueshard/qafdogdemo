@extends('layouts.app')

@section('content')
	<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Document Details</div>

                <div class="panel-body">
                    <form action="{{ action('DocumentController@preview')}}" method="GET" target="_blank">
                    	{{ csrf_field() }}
                        @if($changedate)
							<div class="form-group">
							    <label for="changedate">Change Date:</label>
							    <input type="date" autofocus class="form-control" name="changedate" id="changedate" value="{{ date('Y-m-d')}}">
							  </div>
                        @endif
							<div class="form-group">
							    <label for="PO">Purchase Order Number:</label>
							    <input type="text" autofocus class="form-control" name="PO" id="PO" value="">
							  </div>
							<div class="form-group">
							    <label for="PN">Part Number:</label>
							    <input type="text" class="form-control" name="PN" id="PN" value="">
							  </div>
							<div class="form-group">
							    <label for="revision">Revision:</label>
							    <input type="text" class="form-control" name="revision" Revision:id="revision" Revision:value="">
							  </div>
                        	<div class="form-group">
                        	    <label for="manufacture">Manufacturer:</label>
                        	    
								<select name="manufacture" id="manufacture" class="form-control">
									@foreach($manufactures as $manufacture)
										<option value="{{ $manufacture->id }}">{{ $manufacture->manuname }}</option>
									@endforeach
									
								</select>
                        	  </div>
                        	<div class="form-group">
                        	    <label for="material">Material Designation:</label>
	                        	    <select class="form-control" name="material" id="material">
										@foreach($materials as $material)
											<option value="{{ $material->id }}">{{$material->full()}}</option>
										@endforeach

	                        	    </select>
                        	  </div>
                        @if($shelflife)
							<div class="form-group">
							    <label for="shelflife">Shelf Life:</label>
							    <input type="text" class="form-control" name="shelflife" id="shelflife" value="">
							  </div>
                        @endif
                        @if($linfield)
							<div class="form-group">
							    <label for="linfield">LIN:</label>
							    <input type="text" class="form-control" name="linfield" id="linfield" value="">
							  </div>
                        @endif
                        @if($fandf)
                        	<input type="hidden" name="fandf" id="fandf" value="true">
                        @endif
                        @if($type === "ul")

                        @endif
                        @if($type === "cofc" || $type === 'rohs')
							<div class="form-group">
							    <label for="customer">Customer:</label>
								    <select class="form-control" name="customer" id="customer">
								    	@foreach($customers as $customer)
											<option value="{{ $customer->customer }}">{{ $customer->customer }}</option>
								    	@endforeach
								    </select>
							  </div>
							  <div class="form-group">
							      <label for="quantity">Quantity:</label>
							      <input type="text" class="form-control" name="quantity" id="quantity" value="">
							    </div>
                        @endif
                        <input type="hidden" name='type' value='{{ $type }}'>
                       	<input type="submit" class='btn btn-success' name='doc_type' value="Print Document">
                        
                        <!-- <button class="btn btn-primary" disabled>Print Document</button> -->
                        <!-- <input type="submit" value="Email Document" name='doc_type' class='btn btn-warning'> -->
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
