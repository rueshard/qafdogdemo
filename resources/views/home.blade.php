@extends('layouts.app')

@section('content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <form action="{{ route('document.create')}}" method="GET">
                        
                        <div class="row">
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="type" id="optionsRadios1" value="ul" checked>
                                        Standard UL
                                    </label>
                                </div>
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="type" id="optionsRadios2" value="cofc">
                                    Standard CofC
                                  </label>
                                </div>
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="type" id="optionsRadios3" value="rohs">
                                    ROHS CofC
                                  </label>
                                </div>     
                            </div>
                            <div class="col-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id='shelflife' name='shelflife' value="true">
                                        Shelf Life
                                    </label>
                                </div>
                                <!-- <div class="checkbox">
                                    <label>
                                        <input type="checkbox" disabled id='changedate' name='changedate' value="true">
                                        Change Date
                                    </label>
                                </div> -->
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="fandf" name='fandf' value="true">
                                        F and F Statment
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="linfield" name='linfield' value="true">
                                        LIN
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button type='submit'class="btn btn-primary">Create Document</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
