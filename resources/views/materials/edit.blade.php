@extends('layouts.app')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Edit this Material</h3>
		</div>
		<div class="panel-body">
			@if (count($errors) > 0)
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			<form action="/materials/{{$material->id }}" method="POST" style="display:inline;">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				
					<div class="form-group">
					    <label for="type">Material Type:</label>
					    <input type="text" class="form-control" id="type" name="type" value="{{ $material->type  }}">
					</div>
					<div class="form-group">
					    <label for="grade">Material Grade:</label>
					    <input type="text" class="form-control" id="grade" name="grade" value="{{ $material->grade  }}">
					</div>
					<div class="form-group">
					    <label for="color">Material Color:</label>
					    <input type="text" class="form-control" id="color" name="color" value="{{ $material->color  }}">
					</div>
					<div class="form-group">
					    <label for="notes">Additional Comments:</label>
					    <textarea name="notes" id="notes" class="form-control" cols="30" rows="10">{{ $material->notes  }}</textarea>
					</div>
					<a href="/materials" class="btn btn-warning">< Back</a>
					<button type="Submit" class="btn btn-primary">Update</button>
					
				
			</form>
			<form action="/materials/{{ $material->id }}" method="POST" style="display:inline;">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								
									<button type="Submit" class="btn btn-danger">Delete</button>
								
									
							</form>
		</div>
	</div>
@endsection