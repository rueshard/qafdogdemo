@extends('layouts.app')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Create New Material</h3>
		</div>
		<div class="panel-body">
			@if (count($errors) > 0)
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			<form action="/materials" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
				    <label for="type">Material Type:</label>
				    <input type="text" class="form-control" id="type" name="type" value="">
				</div>
				<div class="form-group">
				    <label for="grade">Material Grade:</label>
				    <input type="text" class="form-control" id="grade" name="grade" value="">
				</div>
				<div class="form-group">
				    <label for="color">Material Color:</label>
				    <input type="text" class="form-control" id="color" name="color" value="">
				</div>
				<div class="form-group">
				    <label for="notes">Additional Comments:</label>
				    <textarea name="notes" id="notes" class="form-control" cols="30" rows="10"></textarea>
				</div>
				<button type="Submit" class="btn btn-primary">Create</button>
			</form>
		</div>
	</div>
	
@endsection