@extends('layouts.app')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">All Materials</h3>
		</div>
		<div class="panel-body">
			<a href="/materials/create" class="btn btn-primary">Add New Material</a>
			<hr>
			@foreach($materials as $material)
				<li><a href="/materials/{{ $material->id }}/edit">{{ $material->type }} {{ $material->grade }} {{ $material->color }} {{ $material->notes }}</a></li>
			@endforeach
			{{ $materials->links() }}
		</div>
	</div>
	
@endsection