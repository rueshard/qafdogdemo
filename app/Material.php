<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $fillable = ['type', 'grade', 'color', 'notes'];

    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = strtoupper(trim($value));
    }
    public function setGradeAttribute($value)
    {
        $this->attributes['grade'] = strtoupper(trim($value));
    }
    public function setColorAttribute($value)
    {
        $this->attributes['color'] = strtoupper(trim($value));
    }
    public function setNotesAttribute($value)
    {
        $this->attributes['notes'] = strtoupper(trim($value));
    }
    public function full()
    {
    	return $this->type . " " . $this->grade . " " . $this->color . " " . $this->notes;
    }
}
