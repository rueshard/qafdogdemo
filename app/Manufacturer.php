<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    protected $fillable = ['manuname'];

    public function setManunameAttribute($value)
    {
        $this->attributes['manuname'] = strtoupper(trim($value));
    }
}
