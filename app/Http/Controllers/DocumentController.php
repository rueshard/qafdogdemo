<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;
use App\Material;
use App\Manufacturer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function create(Request $request)
    {
    	
    	$type = $request->type;
    	$manufactures = Manufacturer::orderby('manuname')->get();
        $customers = Customer::orderby('customer')->get();
        $materials = Material::orderby('type')->get();
    	$shelflife = $request->shelflife;
        $linfield = $request->linfield;
        $fandf = $request->fandf;
    	$changedate = $request->changedate;
    	return view('documents.create', compact('type', 'shelflife', 'linfield', 'fandf', 'changedate', 'manufactures', 'customers', 'materials'));
    }
    public function preview(Request $request)
    {
        
        $manufacturerinfo = Manufacturer::findOrFail($request->manufacture);
        $manu = substr($manufacturerinfo->manuname,4);
        $material = Material::FindOrFail($request->material);
        $data = [
            'date' => date('m/d/Y'),
            'PO' => strtoupper($request->PO),
            'PN' => strtoupper($request->PN),
            'revision' => strtoupper($request->revision),
            'material' => $material->full(),
            'quantity' => $request->quantity,
            'shelflife' => strtoupper($request->shelflife),
            'linfield' => strtoupper($request->linfield),
            'fandf' => $request->fandf,
            'customer' => $request->customer,
            'manufactureedit' => trim($manu),
            'name' => \Auth::user()->name
        ];
        if($request->doc_type == 'Print Document')
        {
            if($request->type === 'ul'){
               return view('certs.ul', compact('data')); 
            }
            if($request->type === 'rohs'){
                return view('certs.rohs', compact('data'));
            }
            return view('certs.cofc', compact('data'));
        }elseif($request->doc_type == 'Email Document'){
            $data['subject'] = "Document for PO: ". $request->PO . " PN: ". $request->PN;
            return view('documents.email', compact('data'));
        }else{

        }    
        
    }
}
