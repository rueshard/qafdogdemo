<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manufacturer;
use App\Http\Requests;

class ManufacturersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $manufacturers = Manufacturer::orderby('manuname', 'asc')->paginate('25');
        return view('manufacturers.index', compact('manufacturers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manufacturers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $this->validate($request, [
            'manucode' => 'required|max:3|min:3',
            'manuname' => 'required|max:255',
        ]);

        $code = strtoupper($request->get('manucode'));
        $name = strtoupper($request->get('manuname'));
        $combine = $code . '-' .$name;
        $manufacturer = new Manufacturer;

        $manufacturer->manuname = $combine;
        $manufacturer->save();
        return redirect('/manufacturers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manufacturer = Manufacturer::FindOrFail($id);

        $manucode = substr($manufacturer->manuname,0,3);
        $manufacturersName = substr($manufacturer->manuname, 4);

        return view('manufacturers.edit', compact('manufacturer', 'manucode', 'manufacturersName'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'manucode' => 'required|max:3|min:3',
            'manuname' => 'required|max:255',
        ]);
        $code = strtoupper($request->get('manucode'));
        $name = strtoupper($request->get('manuname'));
        $combine = $code . '-' .$name;
        $manufacturer =  Manufacturer::FindOrFail($id);

        $manufacturer->manuname = $combine;
        $manufacturer->save();
        return redirect('/manufacturers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteme = Manufacturer::FindOrFail($id);
        $deleteme->delete();
        return redirect('/manufacturers');
    }
}
