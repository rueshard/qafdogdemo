<?php



Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/document/preview', 'DocumentController@preview');
    Route::resource('document', 'DocumentController');
    Route::get('/', 'HomeController@landing');
    Route::get('/home', 'HomeController@index');
    Route::resource('customers', 'CustomersController');
    Route::resource('manufacturers', 'ManufacturersController');
    Route::resource('materials', 'MaterialsController');
    
});
