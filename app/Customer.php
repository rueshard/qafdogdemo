<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['customer'];

     public function setcustomerAttribute($value)
    {
        $this->attributes['customer'] = strtoupper(trim($value));
    }
}
